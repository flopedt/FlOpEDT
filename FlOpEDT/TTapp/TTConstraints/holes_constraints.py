# -*- coding: utf-8 -*-

# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.


import datetime
from django.db import models
from django.utils.translation import gettext_lazy as _
from typing import TYPE_CHECKING

from base.timing import Time
from base.models import StructuralGroup, Week
from people.models import Tutor

if TYPE_CHECKING:
    from TTapp.TTModel import TTModel
from TTapp.ilp_constraints.constraint import Constraint
from TTapp.ilp_constraints.constraint_type import ConstraintType
from TTapp.slots import days_filter, slots_filter
from TTapp.TTConstraints.TTConstraint import TTConstraint
from TTapp.TTConstraints.tutors_constraints import considered_tutors
from TTapp.TTConstraints.groups_constraints import considered_basic_groups
from TTapp.TTConstraints.orsay_constraints import GroupsLunchBreak, TutorsLunchBreak

SLOT_PAUSE = 5


############################
# Tools for hole analysis
############################


def holes_number(ttmodel: "TTModel", tutor_or_group):
    if isinstance(tutor_or_group, Tutor):
        is_busy = ttmodel.IBS
    elif isinstance(tutor_or_group, StructuralGroup):
        is_busy = ttmodel.group_busy_slot
    avail_slots = ttmodel.wdb.availability_slots
    last_slots_number = ttmodel.add_expr()
    for i, slot in enumerate(avail_slots):
        successor = avail_slots[i + 1] if i + 1 < len(avail_slots) else None
        if successor:
            if slot.end_time != successor.start_time:
                continue
            # add if slot is busy and succesor is not busy
            last_slots_number += ttmodel.add_floor(
                is_busy[tutor_or_group, slot] - is_busy[tutor_or_group, successor], 1, 2
            )
        else:
            # add if slot is busy
            last_slots_number += is_busy[tutor_or_group, slot]
    return last_slots_number - ttmodel.one_var


class LimitHoleTime(TTConstraint):
    """
    Limit the total number of holes in each day, and every week
    """

    max_hole_time_per_half_day = models.DurationField(null=True, blank=True)
    max_hole_time_per_day = models.DurationField(null=True, blank=True)
    max_hole_time_per_week = models.DurationField(null=True, blank=True)
    lunch_time_excluded = models.BooleanField(default=True)

    class Meta:
        abstract = True
        verbose_name = _("Limit hole time")
        verbose_name_plural = verbose_name

    def get_viewmodel(self):
        raise NotImplementedError

    def one_line_description(self):
        raise NotImplementedError

    def hole_between_slots(
        self, ttmodel, tutor_or_group, sorted_avail_slots_list, slot, other_slot
    ):
        if isinstance(tutor_or_group, Tutor):
            is_busy = ttmodel.IBS
        elif isinstance(tutor_or_group, StructuralGroup):
            is_busy = ttmodel.group_busy_slot
        hole_between_slots = ttmodel.add_var()
        i = sorted_avail_slots_list.index(slot)
        j = sorted_avail_slots_list.index(other_slot)
        # if hole_between_slots is 1, then both slots are busy
        ttmodel.add_constraint(
            2 * hole_between_slots
            - is_busy[tutor_or_group, slot]
            - is_busy[tutor_or_group, other_slot],
            "<=",
            0,
            Constraint(constraint_type=ConstraintType.HOLE_TIME),
        )

        # if hole_between_slots is 1, then all slots between are free
        gap = j - i - 1
        ttmodel.add_constraint(
            gap * hole_between_slots
            + ttmodel.sum(
                is_busy[tutor_or_group, sorted_avail_slots_list[k]]
                for k in range(i + 1, j)
            ),
            "<=",
            gap,
            Constraint(constraint_type=ConstraintType.HOLE_TIME),
        )

        # if hole_between_slots is 0, then some slots between are busy OR one of the border slots is free
        ttmodel.add_constraint(
            is_busy[tutor_or_group, slot]
            + is_busy[tutor_or_group, other_slot]
            - hole_between_slots
            - ttmodel.sum(
                is_busy[tutor_or_group, sorted_avail_slots_list[k]]
                for k in range(i + 1, j)
            ),
            "<=",
            1,
            Constraint(constraint_type=ConstraintType.HOLE_TIME),
        )
        return hole_between_slots

    def hole_time_expression(self, ttmodel: "TTModel", tutor_or_group, week):
        avail_slots = slots_filter(ttmodel.wdb.availability_slots, week=week)
        hole_time = {week: ttmodel.lin_expr()}
        for d in days_filter(ttmodel.wdb.days, week=week):
            hole_time[d] = {Time.AM: ttmodel.lin_expr(), Time.PM: ttmodel.lin_expr()}
            morning_avail_slots_list = list(
                slots_filter(avail_slots, day=d, apm=Time.AM)
            )
            afternoon_avail_slots_list = list(
                slots_filter(avail_slots, day=d, apm=Time.PM)
            )
            morning_avail_slots_list.sort(key=lambda x: x.start_time)
            afternoon_avail_slots_list.sort(key=lambda x: x.start_time)
            day_avail_slots_list = morning_avail_slots_list + afternoon_avail_slots_list

            for apm in [Time.AM, Time.PM]:
                if apm == Time.AM:
                    avail_slots_list = morning_avail_slots_list
                else:
                    avail_slots_list = afternoon_avail_slots_list
                for i, slot in enumerate(avail_slots_list):
                    for j in range(i + 2, len(avail_slots_list)):
                        other_slot = avail_slots_list[j]
                        hole_between_slots = self.hole_between_slots(
                            ttmodel,
                            tutor_or_group,
                            avail_slots_list,
                            slot,
                            other_slot,
                        )
                        hole_time[d][apm] += hole_between_slots * (
                            other_slot.start_time - slot.end_time
                        )
            hole_time[d] = hole_time[d][Time.AM] + hole_time[d][Time.PM]
            lunch_length = 0
            if self.lunch_time_excluded:
                if isinstance(tutor_or_group, Tutor):
                    lunch_constraints = TutorsLunchBreak.objects.filter(
                        models.Q(weeks=week) | models.Q(weeks=None),
                        models.Q(tutors=tutor_or_group) | models.Q(tutors=None),
                        models.Q(weekdays__contains=[d.day]) | models.Q(weekdays=[]),
                        weight=None,
                        department=self.department,
                    )
                else:
                    lunch_constraints = GroupsLunchBreak.objects.filter(
                        models.Q(weeks=week) | models.Q(weeks=None),
                        models.Q(groups=tutor_or_group) | models.Q(groups=None),
                        models.Q(weekdays__contains=[d.day]) | models.Q(weekdays=[]),
                        weight=None,
                        department=self.department,
                    )
                if lunch_constraints.exists():
                    lunch_length = max(l_c.lunch_length for l_c in lunch_constraints)

            for i, slot in enumerate(morning_avail_slots_list):
                for other_slot in afternoon_avail_slots_list:
                    if other_slot == day_avail_slots_list[i + 1]:
                        continue
                    hole_between_slots = self.hole_between_slots(
                        ttmodel,
                        tutor_or_group,
                        day_avail_slots_list,
                        slot,
                        other_slot,
                    )
                    if other_slot.start_time - slot.end_time - lunch_length > 0:
                        hole_time[d] += hole_between_slots * (
                            other_slot.start_time - slot.end_time - lunch_length
                        )
            hole_time[week] += hole_time[d]
        return hole_time

    def enrich_model_for_one_object(self, ttmodel, tutor_or_group, week, ponderation):
        if isinstance(tutor_or_group, Tutor):
            kwargs = {"instructors": tutor_or_group}
            add_cost_method = ttmodel.add_to_inst_cost
        elif isinstance(tutor_or_group, StructuralGroup):
            kwargs = {"groups": tutor_or_group}
            add_cost_method = ttmodel.add_to_group_cost

        hole_time = self.hole_time_expression(ttmodel, tutor_or_group, week)
        if self.max_hole_time_per_half_day:
            for d in days_filter(ttmodel.wdb.days, week=week):
                for apm in [Time.AM, Time.PM]:
                    if self.weight is None:
                        ttmodel.add_constraint(
                            hole_time[d][apm],
                            "<=",
                            self.max_hole_time_per_half_day.total_seconds() / 60,
                            Constraint(
                                constraint_type=ConstraintType.HOLE_TIME_PER_HALF_DAY,
                                days=d,
                                apm=apm,
                                **kwargs,
                            ),
                        )
                    else:
                        cost = ponderation * self.local_weight()
                        for i in range(1, 5):
                            unwanted = ttmodel.add_floor(
                                hole_time[d][apm],
                                i * self.max_hole_time_per_half_day.total_seconds() / 60
                                + 1,
                                10000,
                            )
                            add_cost_method(
                                tutor_or_group,
                                unwanted * cost,
                                week,
                            )
                            cost *= 2

        if self.max_hole_time_per_day:
            if self.weight is None:
                for d in days_filter(ttmodel.wdb.days, week=week):
                    ttmodel.add_constraint(
                        hole_time[d],
                        "<=",
                        self.max_hole_time_per_day.total_seconds() / 60,
                        Constraint(
                            constraint_type=ConstraintType.HOLE_TIME_PER_DAY,
                            days=d,
                            **kwargs,
                        ),
                    )
            else:
                cost = ponderation * self.local_weight()
                for i in range(1, 5):
                    unwanted = ttmodel.add_floor(
                        hole_time[d],
                        i * self.max_hole_time_per_day.total_seconds() / 60 + 1,
                        10000,
                    )
                    add_cost_method(tutor_or_group, unwanted * cost, week)
                    cost *= 2
        if self.max_hole_time_per_week:
            if self.weight is None:
                ttmodel.add_constraint(
                    hole_time[week],
                    "<=",
                    self.max_hole_time_per_week.total_seconds() / 60,
                    Constraint(
                        constraint_type=ConstraintType.HOLE_TIME_PER_WEEK,
                        weeks=week,
                        **kwargs,
                    ),
                )
            else:
                cost = ponderation * self.local_weight()
                for i in range(1, 5):
                    unwanted = ttmodel.add_floor(
                        hole_time[week],
                        i * self.max_hole_time_per_week.total_seconds() / 60 + 1,
                        10000,
                    )
                    add_cost_method(tutor_or_group, unwanted * cost, week)
                    cost *= 2

    def enrich_ttmodel(self, ttmodel: "TimetableModel", week: "Week", ponderation=1):
        raise NotImplementedError


class LimitTutorsHoleTime(LimitHoleTime):
    tutors = models.ManyToManyField("people.Tutor", blank=True)

    class Meta:
        verbose_name = _("Limit tutors hole time")
        verbose_name_plural = verbose_name

    def get_viewmodel(self):
        view_model = super().get_viewmodel()
        details = view_model["details"]

        if self.groups.exists():
            details.update(
                {"tutors": ", ".join([tutor.username for tutor in self.tutors.all()])}
            )

        return view_model

    def one_line_description(self):
        text = "Limite le nombre de trous "
        if self.max_hole_time_per_day is not None:
            text += f"à {self.max_hole_time_per_day} par jour "
            if self.max_hole_time_per_period is not None:
                text += f"et "
        if self.max_hole_time_per_period is not None:
            text += f"à {self.max_hole_time_per_period} par période "
        if self.tutors.exists():
            text += " pour : " + ", ".join(
                [tutor.username for tutor in self.tutors.all()]
            )
        else:
            text += " pour tous les profs"

    def enrich_ttmodel(self, ttmodel, period, ponderation=1):
        for tutor in considered_tutors(self, ttmodel):
            self.enrich_model_for_one_object(ttmodel, tutor, period, ponderation)


class LimitGroupsHoleTime(LimitHoleTime):
    """
    Limit the total number of holes in each day, and every week
    """

    train_progs = models.ManyToManyField("base.TrainingProgramme", blank=True)
    groups = models.ManyToManyField("base.StructuralGroup", blank=True)

    class Meta:
        verbose_name = _("Limit groups hole time")
        verbose_name_plural = verbose_name

    def get_viewmodel(self):
        view_model = super().get_viewmodel()
        details = view_model["details"]

        if self.groups.exists():
            details.update(
                {"groups": ", ".join([group.name for group in self.groups.all()])}
            )
        if self.train_progs.exists():
            details.update(
                {
                    "train_progs": ", ".join(
                        [train_prog.abbrev for train_prog in self.train_progs.all()]
                    )
                }
            )

        return view_model

    def one_line_description(self):
        text = "Limite le nombre de trous "
        if self.max_hole_time_per_day is not None:
            text += f"à {self.max_hole_time_per_day} par jour "
            if self.max_hole_time_per_period is not None:
                text += f"et "
        if self.max_hole_time_per_period is not None:
            text += f"à {self.max_hole_time_per_period} par période "
        if self.groups.exists():
            text += " pour : " + ", ".join([group.name for group in self.groups.all()])
        else:
            text += " pour tous les groupes"
        if self.train_progs.exists():
            text += " en " + ", ".join(
                [train_prog.abbrev for train_prog in self.train_progs.all()]
            )
        return text

    def enrich_ttmodel(self, ttmodel, period, ponderation=1):
        for bg in considered_basic_groups(self, ttmodel):
            self.enrich_model_for_one_object(ttmodel, bg, period, ponderation)
