# -*- coding: utf-8 -*-

# This file is part of the FlOpEDT/FlOpScheduler project.
# Copyright (c) 2017
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
#
# You can be released from the requirements of the license by purchasing
# a commercial license. Buying such a license is mandatory as soon as
# you develop activities involving the FlOpEDT/FlOpScheduler software
# without disclosing the source code of your own applications.


from django.contrib.postgres.fields import ArrayField
from base.timing import Day, TimeInterval, flopdate_to_datetime
from datetime import datetime
from base.partition import Partition
from base.models import Week
from people.models import Tutor

from django.db import models
from django.db.models import Q


from django.core.exceptions import ObjectDoesNotExist

from base.timing import french_format
from base.timing import Day

from TTapp.ilp_constraints.constraint_type import ConstraintType
from TTapp.ilp_constraints.constraint import Constraint
from TTapp.slots import days_filter, slots_filter
from TTapp.TTConstraints.TTConstraint import TTConstraint
from TTapp.TTConstraints.groups_constraints import considered_basic_groups
from TTapp.slots import Slot
from TTapp.TTConstraints.tutors_constraints import considered_tutors
from django.utils.translation import gettext_lazy as _


class AvoidBothGroupsSimultaneously(TTConstraint):
    """
    Ensures that groups of one set and the other are not scheduled simultaneously
    """

    first_groups = models.ManyToManyField(
        "base.StructuralGroup",
        related_name="as_first_group_avoid_both_groups_constraints",
    )
    second_groups = models.ManyToManyField(
        "base.StructuralGroup",
        related_name="as_second_group_avoid_both_groups_constraints",
    )
    weekdays = ArrayField(
        models.CharField(max_length=2, choices=Day.CHOICES),
    )

    class Meta:
        verbose_name = _("Avoid both groups simultaneously")
        verbose_name_plural = verbose_name

    @classmethod
    def get_viewmodel_prefetch_attributes(cls):
        attributes = super().get_viewmodel_prefetch_attributes()
        attributes.extend(["first_groups", "second_groups"])
        return attributes

    def enrich_ttmodel(self, ttmodel, week, ponderation=100):
        if not self.first_groups.exists() or not self.second_groups.exists():
            return
        first_groups_courses = {"all": set()}
        for group in self.first_groups.all():
            group_courses = ttmodel.wdb.courses_for_group[group]
            first_groups_courses["all"] |= group_courses
            first_groups_courses[group] = group_courses
        second_groups_courses = {"all": set()}
        for group in self.second_groups.all():
            group_courses = ttmodel.wdb.courses_for_group[group]
            second_groups_courses["all"] |= group_courses
            second_groups_courses[group] = group_courses

        considered_availability_slots = slots_filter(
            ttmodel.wdb.availability_slots, week=week
        )
        if self.weekdays:
            considered_availability_slots = slots_filter(
                considered_availability_slots, week_day_in=self.weekdays
            )
        for slot in considered_availability_slots:
            if self.weight is None:
                are_first_groups_scheduled = ttmodel.add_floor(
                    ttmodel.sum(
                        ttmodel.TT[sl, c]
                        for c in first_groups_courses["all"]
                        for sl in slots_filter(
                            ttmodel.wdb.compatible_slots[c], simultaneous_to=slot
                        )
                    ),
                    1,
                    len(first_groups_courses["all"]) + 1,
                )
                are_second_groups_scheduled = ttmodel.add_floor(
                    ttmodel.sum(
                        ttmodel.TT[sl, c]
                        for c in second_groups_courses["all"]
                        for sl in slots_filter(
                            ttmodel.wdb.compatible_slots[c], simultaneous_to=slot
                        )
                    ),
                    1,
                    len(second_groups_courses["all"]) + 1,
                )
                ttmodel.add_constraint(
                    are_first_groups_scheduled + are_second_groups_scheduled,
                    "<=",
                    1,
                    Constraint(
                        constraint_type=ConstraintType.AVOID_BOTH_GROUPS_SIMULTANEOUSLY,
                        weeks=week,
                        slots=slot,
                    ),
                )
            else:
                for g1 in self.first_groups.all():
                    for g2 in self.second_groups.all():
                        are_first_groups_scheduled = ttmodel.add_floor(
                            ttmodel.sum(
                                ttmodel.TT[sl, c]
                                for c in first_groups_courses[g1]
                                for sl in slots_filter(
                                    ttmodel.wdb.compatible_slots[c],
                                    simultaneous_to=slot,
                                )
                            ),
                            1,
                            len(first_groups_courses[g1]) + 1,
                        )
                        are_second_groups_scheduled = ttmodel.add_floor(
                            ttmodel.sum(
                                ttmodel.TT[sl, c]
                                for c in second_groups_courses[g2]
                                for sl in slots_filter(
                                    ttmodel.wdb.compatible_slots[c],
                                    simultaneous_to=slot,
                                )
                            ),
                            1,
                            len(second_groups_courses[g2]) + 1,
                        )
                        both = ttmodel.add_floor(
                            are_first_groups_scheduled + are_second_groups_scheduled,
                            2,
                            100,
                        )
                        ttmodel.add_to_generic_cost(
                            self.local_weight() * ponderation * both, week
                        )

    def one_line_description(self):
        return (
            f"Avoid that groups {', '.join(str(g) for g in self.first_groups.all())}"
            f"and '{', '.join(str(g) for g in self.second_groups.all())}' are scheduled simultaneously"
        )
