# Copyright (c) 2017-present, flop! contributors
# SPDX-License-Identifier: AGPL-3.0-or-later
# https://framagit.org/flopedt/flopedt

import datetime as dt
import random

from displayweb.models import TrainingProgrammeDisplay

from base.models import (
    Course,
    CourseStartTimeConstraint,
    CourseType,
    Department,
    Dependency,
    GroupType,
    Mode,
    Module,
    ModulePossibleTutors,
    Room,
    RoomType,
    ScheduledCourse,
    Week,
    StructuralGroup,
    TimeGeneralSettings,
    Period,
    TrainingProgramme,
    TransversalGroup,
    UserPreference,
    CoursePreference
)
from people.models import FullStaff, SupplyStaff, Tutor, TutorPreference
from misc.assign_colors import assign_module_color

start_time_constraints = {
    60: [h*60 for h in range(8, 19)],
    120: [h*60 for h in range(8, 18, 1)],
    90: [
        8*60 + i*90 for i in range(7)
    ],
}

allowed_weekdays = ["m", "tu", "w", "th", "f"]

course_type_names = ["CM60", 'CM90', "CM120", "TD60", 'TD90','TD120',"TP60", 'TP90', 'TP120']

courses_dic = {
    "TP1": {
        "mod1": {"CM": (60, 1), "TD": (120, 2, "ACM"), "TP": (120, 1), "tutors": [1, 2, 3, 4, 5]},
        "mod2": {"CM": (60, 1), "TD": (90, 3), "TP": (90, 2, "ATD", "D"), "tutors": [4, 5, 6]},
        "mod3": {"CM": (60, 1), "TD": (90, 3), "TP": (90, 2, "ATD"), "tutors": [7, 8, 9]},
    },
    "TP2": {
        "mod4": {"CM": (60, 0), "TD": (60, 5), "TP": (60, 1), "tutors": [1, 3, 4, 7]},
        "mod5": {"CM": (60, 2), "TD": (90, 3), "TP": (90, 1), "tutors": [2, 4, 8]},
        "mod6": {"CM": (90, 1), "TD": (120, 2), "TP": (120, 1), "tutors": [3, 5, 9]},
    },
}


def make_dept_and_needs():
    T, _= Tutor.objects.get_or_create(username="MOI", is_staff=True, is_superuser=True)
    T.set_password("passe")
    T.save()
    d, _ = Department.objects.get_or_create(name="dept_name", abbrev="abbrev")
    t, _ = TimeGeneralSettings.objects.get_or_create(department=d)
    t.day_start_time=8*60
    t.day_finish_time=20*60
    t.lunch_break_start_time=13*60
    t.lunch_break_finish_time=13*60
    t.days = allowed_weekdays
    t.save()
    Mode.objects.get_or_create(department=d)
    training_period, _ = Period.objects.get_or_create(department=d, name="Period test", starting_week=1, ending_week=1)
    for p in range(1, 53):
        Week.objects.get_or_create(nb= p, year=2024)
        Week.objects.get_or_create(nb= p, year=2025)

    for t in course_type_names:
        CourseType.objects.get_or_create(department=d, name=t, duration = int(t[2:]))
    for duration, allowed_start_times in start_time_constraints.items():
        for ct in ['TD', 'TP', 'CM']:
            CourseStartTimeConstraint.objects.get_or_create(
                course_type=CourseType.objects.get(department=d, name=ct+str(duration)),
                allowed_start_times=allowed_start_times,
            )


def make_modules():
    train_period = Period.objects.first()
    for num,tp in enumerate(TrainingProgramme.objects.all()):
        for i in range(1, 4):
            Module.objects.get_or_create(
                name=f"module_{3*num+i}",
                abbrev=f"mod{3*num+i}",
                train_prog=tp,
                period=train_period,
            )


def make_classical_groups_structure():
    department = Department.objects.first()
    cm_type, _ = GroupType.objects.get_or_create(name="CM", department=department)
    td_type, _ = GroupType.objects.get_or_create(name="TD", department=department)
    tp_type, _ = GroupType.objects.get_or_create(name="TP", department=department)
    for i in range(1, 3):
        tp, _ = TrainingProgramme.objects.get_or_create(
            name=f"TrainProg{i}",
            abbrev=f"TP{i}",
            department=Department.objects.first(),
        )
        TrainingProgrammeDisplay.objects.get_or_create(training_programme= tp, row=0)
        groups = []
        groups.append(
            StructuralGroup.objects.get_or_create(train_prog=tp, name="CM", type=cm_type, size=0)[0],
        )
        for i in range(1, 3):
            group,_ = StructuralGroup.objects.get_or_create(
                train_prog=tp,
                name=f"TD{i}",
                type=td_type,
                size=0,
            )
            group.parent_groups.add(groups[0])
            for letter in "AB":
                subgroup,_  = StructuralGroup.objects.get_or_create(
                    train_prog=tp,
                    name=f"TP{i}{letter}",
                    basic=True,
                    type=tp_type,
                    size=0,
                )
                subgroup.parent_groups.add(group)
                groups.append(subgroup)
            groups.append(group)
        tr_12a, _ = TransversalGroup.objects.get_or_create(train_prog=tp, name="Transversal 1&2A", size=0)
        tr_12a.conflicting_groups.add(StructuralGroup.objects.get(name="TD1", train_prog=tp))
        tr_12a.conflicting_groups.add(StructuralGroup.objects.get(name="TP2A", train_prog=tp))
        tr_12a.save()
        tr_1b2,_ = TransversalGroup.objects.get_or_create(train_prog=tp, name="Transversal 1B&2", size=0)
        tr_1b2.conflicting_groups.add(StructuralGroup.objects.get(name="TP1B", train_prog=tp))
        tr_1b2.conflicting_groups.add(StructuralGroup.objects.get(name="TD2", train_prog=tp))
        tr_1b2.parallel_groups.add(tr_12a)
        tr_1b2.save()


def make_rooms():
    d = Department.objects.get(abbrev="abbrev")
    n = 15
    n = max(4, n)
    q = n // 4
    a,_ = RoomType.objects.get_or_create(name="CM", department=d)
    td,_ = RoomType.objects.get_or_create(name="TD", department=d)
    tp,_ = RoomType.objects.get_or_create(name="TP", department=d)
    for i in range(1,q+1):
        r,_ = Room.objects.get_or_create(name=f"Amphi_{i}")
        r.departments.add(d)
        a.members.add(r)
    for i in range(1,q+1):
        r,_ = Room.objects.get_or_create(name=f"TD_{i}")
        r.departments.add(d)
        td.members.add(r)
    for i in range(1,q+1):
        r,_ = Room.objects.get_or_create(name=f"TDP_{i}")
        r.departments.add(d)
        td.members.add(r)
        tp.members.add(r)
    for i in range(1,n - 3 * q+1):
        r,_ = Room.objects.get_or_create(name=f"TP_{i}")
        r.departments.add(d)
        tp.members.add(r)


def make_tutors_pref_and_av():
    full_staffs = []
    d = Department.objects.get(abbrev="abbrev")
    for i in range(1,6):
        fs,_ = FullStaff.objects.get_or_create(username=f"Tutor_{i}")
        fs.departments.add(d)
        full_staffs.append(fs)
    supply_staffs = []
    for i in range(1,6):
        ss,_ = SupplyStaff.objects.get_or_create(username=f"Supply_{i}")
        ss.departments.add(d)
        supply_staffs.append(ss)
    for day in allowed_weekdays:
        for hour in range(8, 20):
            for tutor in full_staffs + supply_staffs:
                TutorPreference.objects.get_or_create(tutor=tutor)
                UserPreference.objects.get_or_create(
                    user=tutor,
                    start_time=hour*60,
                    day=day,
                    duration=60,
                    value=8,
                )
            for tp in TrainingProgramme.objects.all():
                for ct in CourseType.objects.all():
                    CoursePreference.objects.get_or_create(
                        course_type=ct,
                        train_prog=tp,
                        start_time=hour*60,
                        day=day,
                        duration=60,
                        value=8 if hour in range(10, 18) else 2,
                    )



def make_courses():
    print("MAKE COURSES")
    make_dept_and_needs()
    make_classical_groups_structure()
    make_rooms()
    make_tutors_pref_and_av()
    make_modules()
    period = Week.objects.get(nb=1, year=2024)
    d= Department.objects.get(abbrev="abbrev")
    tutors = list(Tutor.objects.filter(departments=d))
    for tp_abbrev, modules_dic in courses_dic.items():
        tp = TrainingProgramme.objects.get(abbrev=tp_abbrev)
        for mod_abbrev, course_type_dic in modules_dic.items():
            m = Module.objects.get(abbrev=mod_abbrev, train_prog=tp)
            module_possible_tutors,_ = ModulePossibleTutors.objects.get_or_create(module=m)
            considered_tutors = [tutors[i] for i in course_type_dic["tutors"]]
            module_possible_tutors.possible_tutors.set(considered_tutors)
            considered_tutors = considered_tutors + [None]
            i = 0
            for rk, course_type_name in enumerate(course_type_names):
                room_type, _ = RoomType.objects.get_or_create(name=course_type_name[:2])
                (duration, nb, *comments) = course_type_dic[course_type_name[:2]]
                if duration != int(course_type_name[2:]):
                    continue
                    
                if nb == 2:
                    comments = (random.choice(["D", "ND", "A" + course_type_names[rk - 1]]),)

                # add a course for some transversal group
                if nb == 3:
                    nb -= 1
                    trans_nb = 2
                    for tg in TransversalGroup.objects.filter(train_prog=tp)[:trans_nb]:
                        course = Course.objects.create(
                            module=m,
                            tutor=considered_tutors[i % len(considered_tutors)],
                            type=CourseType.objects.get(name=course_type_name),
                            room_type=room_type,
                            week=period,
                        )
                        course.groups.add(tg)
                        i += 1 % len(considered_tutors)

                for g in StructuralGroup.objects.filter(
                    train_prog=tp,
                    type__name=course_type_name[:2],
                ):
                    for rank in range(nb):
                        course = Course.objects.create(
                            module=m,
                            tutor=considered_tutors[i % len(considered_tutors)],
                            type=CourseType.objects.get(name=course_type_name),
                            room_type=room_type,
                            week=period,
                        )
                        course.groups.add(g)
                        for comment in comments:
                            if comment.startswith("A"):
                                a_type = comment[1:]
                                firsts = Course.objects.filter(
                                    groups__in=g.ancestor_groups(),
                                    type__name=a_type,
                                )
                                if firsts.exists():
                                    Dependency.objects.create(
                                        course1=firsts.first(),
                                        course2=course,
                                    )
                            elif comment == "D":
                                if rank % 2 == 1:
                                    Dependency.objects.create(
                                        course1=Course.objects.get(id=course.id - 1),
                                        course2=course,
                                        successive=True,
                                    )
                            elif comment == "ND":
                                if rank % 2 == 1:
                                    Dependency.objects.create(
                                        course1=Course.objects.get(id=course.id - 1),
                                        course2=course,
                                        day_gap=1,
                                    )

                        i += 1 % len(considered_tutors)
    assign_module_color(d, True, True)

def make_random_schedule():
    print("MAKE RANDOM SCHEDULE")
    dept = Department.objects.get(abbrev="abbrev")
    period = Week.objects.get(nb=1, year=2024)
    possible_slots = {
        duration: [
            (d, h)
            for d in range(1, len(allowed_weekdays) + 1)
            for h in possible_start_times
        ]
        for duration, possible_start_times in start_time_constraints.items()
    }
    used_slots = {}
    for bg in StructuralGroup.objects.filter(basic=True):
        used_slots[bg] = set()
    for tg in TransversalGroup.objects.all():
        used_slots[tg] = set()
    for t in Tutor.objects.all():
        used_slots[t] = set()
    for r in Room.objects.all():
        used_slots[r] = set()
    for course in Course.objects.all():
        ok = False
        while not ok:
            group = course.groups.first()
            slot = random.choice(list(possible_slots[course.type.duration]))
            if group.is_transversal:
                basic_groups = group.transversalgroup.conflicting_basic_groups()
                if (
                    any(sl.is_simultaneous_to(slot) for sl in used_slots[group.transversalgroup])
                    or any(
                        sl.is_simultaneous_to(slot) for bg in basic_groups for sl in used_slots[bg]
                    )
                    or (
                        course.tutor is not None
                        and any(sl.is_simultaneous_to(slot) for sl in used_slots[course.tutor])
                    )
                ):
                    continue
            else:
                basic_groups = group.structuralgroup.basic_groups()
                if any(
                    sl.is_simultaneous_to(slot) for bg in basic_groups for sl in used_slots[bg]
                ) or (
                    course.tutor is not None
                    and any(sl.is_simultaneous_to(slot) for sl in used_slots[course.tutor])
                ):
                    continue
            tutor = course.tutor
            if tutor is None:
                tutor = random.choice(
                    list(course.module.modulepossibletutors.possible_tutors.all()),
                )
            if not random.choice(range(11)):
                supp_tutor = random.choice(
                    list(course.module.modulepossibletutors.possible_tutors.exclude(id=tutor.id)),
                )
                course.supp_tutors.add(supp_tutor)
            sc = ScheduledCourse.objects.create(
                course=course,
                start_time=slot.start_time,
                tutor=tutor,
                work_copy=0,
            )
            while not ok:
                room = random.choice(list(course.room_type.members.all()))
                if any(sl.is_simultaneous_to(slot) for sl in used_slots[room]):
                    continue
                ok = True
                if group.is_transversal:
                    used_slots[group.transversalgroup].add(slot)
                for bg in basic_groups:
                    used_slots[bg].add(slot)
                if tutor is not None:
                    used_slots[tutor].add(slot)
                used_slots[room].add(slot)
                sc.room = room
                sc.save()


def make_export():
    from base.timing import flopdate_to_datetime, Day
    from base.models import Week
    SC = [str(sc).split('-') for sc in ScheduledCourse.objects.all()]
    result = []
    w = Week.objects.get(nb=1, year=2024)
    for sc in SC:
        if '' in sc:
            sc.remove('')
            sc.remove('')
        course_type = sc[0][:2]
        duration = int(sc[0][2:])
        module = sc[1]
        tutor = sc[2] if sc[2] != 'no_tut' else None
        group = sc[3].split(':')[0]
        weekday = sc[3].split(':')[1]
        startime = flopdate_to_datetime(Day(week=w, day=weekday), int(sc[4][1:]))
        room = sc[5]
        result.append({"course_type" : course_type, "duration" : duration, "module" : module, "tutor" : tutor, "group" : group, 
        "startime" : startime, "room" : room})
    return result

